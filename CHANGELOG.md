<font size ="5"> v1.0.1 - 20230605</font>
<hr style="1px solid">

<ul>
<li>
[JIRA-6] Remove addition <a href="https://gitlab.com/taquanghuy/gitproject/-/merge_requests/8"> PR#8 </a>
</li>
</ul>

<font size ="5"> v1.0.0 - 20230605</font>
<hr style="1px solid">

<ul>
<li>
[JIRA-1] Implement addition <a href="https://gitlab.com/taquanghuy/gitproject/-/merge_requests/1"> PR#1 </a>
</li>
<li>
[JIRA-3] Implement multiplication <a href="https://gitlab.com/taquanghuy/gitproject/-/merge_requests/2"> PR#2 </a>
</li>
<li>
[JIRA-2] Implement subtraction <a href="https://gitlab.com/taquanghuy/gitproject/-/merge_requests/3"> PR#3 </a>
</li>
<li>
[JIRA-4] Implement division <a href="https://gitlab.com/taquanghuy/gitproject/-/merge_requests/4"> PR#4 </a>
</li>
</ul>
